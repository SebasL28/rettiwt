@extends('layouts.app')
@section('title', 'Inicio de sesión')

@section('styles')
<link href="{{ asset('css/login.css') }}" rel="stylesheet">
@endsection

@section('content')
	<div class="container-login">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-12 col-md-5">
					<div style="opacity: 0;" class="card card-login">
						<div class="card-header">
							<ul class="nav"role="tablist">
								<li class="nav-item">
									<a class="nav-link {{ (session('form_validate') == 'login' )? 'active' : '' }}" data-toggle="tab" href="#login" role="tab" aria-controls="login" aria-selected="true">Iniciar sesión</a>
								</li>
								<li class="nav-item">
									<a class="nav-link {{ (session('form_validate') == 'register' )? 'active' : '' }}" data-toggle="tab" href="#register" role="tab" aria-controls="register" aria-selected="false">Registro</a>
								</li>
							</ul>
						</div>
						<div class="card-body tab-content">
							<div class="tab-pane fade {{ (session('form_validate') == 'login' )? 'show active' : '' }}" id="login" role="tabpanel" aria-labelledby="login-tab">
								<div class="img-avatar">
									<img src="/images/avatar-login.svg" alt="User Icon" />
								</div>
								<form class="form-animation" method="POST" action="{{ route('login') }}">
									@csrf
									<div class="form-group row">
										<input type="email" class="form-control {{ ($errors->has('email') && session('form_validate') == 'login') ? ' is-invalid' : '' }}" name="email" value="{{ (session('form_validate') == 'login')? old('email') : '' }}" required {{ (session('form_validate') == 'login')? 'autofocus' : '' }} placeholder="Correo">

										@if ($errors->has('email') && session('form_validate') == 'login')
											<span class="invalid-feedback" role="alert">
												<strong>{{ $errors->first('email') }}</strong>
											</span>
										@endif
									</div>

									<div class="form-group row">
										<input type="password" class="form-control {{ ($errors->has('password') && session('form_validate') == 'login') ? ' is-invalid' : '' }}" name="password" required placeholder="Contraseña">

										@if ($errors->has('password') && session('form_validate') == 'login')
											<span class="invalid-feedback" role="alert">
											<strong>{{ $errors->first('password') }}</strong>
											</span>
										@endif
									</div>

									<div class="form-group row mb-0 justify-content-center">
										<div class="col-12 col-md-8">
											<button type="submit" class="btn btn-block">
											Iniciar sesión
											</button>
										</div>
									</div>
								</form>
							</div>
							<div class="tab-pane fade {{ (session('form_validate') == 'register' )? 'show active' : '' }}" id="register" role="tabpanel" aria-labelledby="register-tab">
								<div class="img-avatar">
									<img src="/images/avatar-login.svg" style="width: 200px;" alt="User Icon" />
								</div>
								<form class="form-animation" method="POST" action="{{ route('register') }}">
									@csrf
									
									<div class="form-group">
										<input type="text" class="form-control {{ ($errors->has('name') && session('form_validate') == 'register') ? ' is-invalid' : '' }}" name="name" value="{{ (session('form_validate') == 'register')? old('name') : '' }}" {{ (session('form_validate') == 'register')? 'autofocus' : '' }} required placeholder="Nombre">
										@if ($errors->has('name') && session('form_validate') == 'register')
											<span class="invalid-feedback" role="alert">
												<strong>{{ $errors->first('name') }}</strong>
											</span>
										@endif
									</div>

									<div class="form-group">
										<input type="email" class="form-control {{ ($errors->has('email') && session('form_validate') == 'register') ? ' is-invalid' : '' }}" name="email" value="{{ (session('form_validate') == 'register')? old('email') : '' }}" required placeholder="Correo">

										@if ($errors->has('email') && session('form_validate') == 'register')
											<span class="invalid-feedback" role="alert">
												<strong>{{ $errors->first('email') }}</strong>
											</span>
										@endif
									</div>

									<div class="form-group">
										<input type="password" class="form-control {{ ($errors->has('password') && session('form_validate') == 'register') ? ' is-invalid' : '' }}" name="password" required placeholder="Contraseña">

										@if ($errors->has('password') && session('form_validate') == 'register')
											<span class="invalid-feedback" role="alert">
											<strong>{{ $errors->first('password') }}</strong>
											</span>
										@endif
									</div>

									<div class="form-group">
										<input type="password" class="form-control" name="password_confirmation" required placeholder="Repetir contraseña">
									</div>

									<div class="form-group row mb-0 justify-content-center">
										<div class="col-12 col-md-8">
											<button type="submit" class="btn btn-block">
											Registrarme
											</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<script>
	$(document).ready(function(){
		var card = $(".card-login");
		
		TweenLite.set(card, {y:-100});
		TweenLite.to(card, 1, {opacity:1, y:0});

		$(".form-animation").submit(function(e){
			e.preventDefault();
			var form = $(this);

			TweenLite.to(card, 1, {opacity:0, y:-100});

			setTimeout(function(){
				form.unbind().submit();
			}, 300);
		});
	});
</script>
@endsection
