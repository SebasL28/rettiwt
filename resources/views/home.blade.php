@extends('layouts.app')

@section('title', 'Inicio')

@section('styles')
<link href="{{ asset('css/home.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container">
	<div class="card card-board">
		<div class="card-header">
			<div class="container-bg-header">
				<img src="/images/banner.jpg">
			</div>
			<div class="profile">
				<div class="info">
					<h3>{{ Auth::user()->name }}</h3>
					<p>{{ Auth::user()->email }}</p>
				</div>
				<div class="avatar-profile">
					<img class="rounded-circle" src="/images/avatar.jpg">
				</div>
			</div>
			<div class="content-logout">
				<a href="#" id="btn-logout" class="btn rounded-circle">
					<span class="fa fa-times"></span>
				</a>
				<form id="form-logout" action="{{ route('logout') }}" method="POST">
				@csrf
				</form>
			</div>
		</div>
		<div class="card-body">
			<div class="row">
				<div class="col-12 col-md-4">
					<div class="card-container">
						<form action="?" id="form-new-post">
							<div class="input-group-post">
								<textarea id="new-post-txt" rows="2" class="form-control" placeholder="¿Qué estás pensando?" maxlength="140" required></textarea>
								<small id="length-new-post"><span>0</span>/140</small>
							</div>
							<button type="submit" class="btn btn-primary btn-block">Enviar</button>
						</form>
					</div>
				</div>
				<div id="container-posts" class="col-12 col-md-8">
					@if($posts->count() > 0)
						@foreach($posts as $post)
						<div class="card-container">
							<article data-id="{{ $post->id }}" class="post">
								<div class="article-header">
									<div class="row">
										<div class="col-8">
											<div class="info-author">
												<div class="avatar-post rounded-circle">
													<img class="img-fluid" src="/images/avatar.jpg">
												</div>
												<p>{{ $post->user->name }}</p>
												<small>{{ $post->user->email }}</small>
											</div>
										</div>
										@if($post->user->id == Auth::user()->id)
										<div class="col-4 text-right">
											<div class="dropdown dropleft">
												<button class="btn btn-menu-post rounded-circle" type="button" data-toggle="dropdown">
													<i class="fa fa-ellipsis-h"></i>
												</button>
												<div class="dropdown-menu">
													<a class="dropdown-item btn-update" href="#">Editar</a>
													<a class="dropdown-item btn-delete" href="#">Eliminar</a>
												</div>
											</div>
										</div>
										@endif
									</div>
								</div>
								<p class="text-post">{{ $post->text }}</p>
							</article>
						</div>
						@endforeach
					@else
					<div class="card-container post-empty">
						<h3 class="text-center">No se encontraron publicaciones</h3>
					</div>
					@endif
				</div>
				<div class="col-12 col-md-8 offset-md-4 text-center">
					<a href="#" id="btn-previous" class="btn btn-info">Cargar posts anteriores</a>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal-update">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-body">
				<form action="?" id="form-update-post">
					<div class="input-group-post">
						<textarea id="update-post-txt" rows="3" class="form-control" placeholder="¿Qué estás pensando?" maxlength="140" required></textarea>
						<small id="length-update-post"><span>0</span>/140</small>
					</div>
					<button type="submit" class="btn btn-warning btn-block">Actualizar post</button>
				</form>
			</div>
		</div>
	</div>
</div>
<script>
	var url_new_post = '{{ route('post.new') }}',
		url_update_post = '{{ route('post.update') }}',
		url_delete_post = '{{ route('post.delete') }}',
		url_load_changes = '{{ route('post.load_changes') }}',
		url_previous = '{{ route('post.previous') }}',
		email_user = '{{Auth::user()->email}}',
		time_load_changes = 1000;
</script>
<script src="/js/home.js"></script>
@endsection
