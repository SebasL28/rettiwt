<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\PostRequest;
use App\Http\Requests\PostUpdateRequest;
use App\Http\Requests\PostDeleteRequest;
use Illuminate\Support\Facades\Auth;
use App\Post;

class PostController extends Controller
{

	private function utf8_strrev($str){
		preg_match_all('/./us', $str, $ar);
		return join('', array_reverse($ar[0]));
	}

	private function reverseFirstWord($text){
		$text = explode(" ", $text);
		$first_word = $text[0];

		array_splice($text, 0, 1);
		$text = implode(" ", $text);

		// $first_word = utf8_encode($first_word);
		$first_word = ucfirst(mb_strtolower($this->utf8_strrev($first_word)));
		$text = $first_word . " " . $text;
		return $text;
	}

	public function store(PostRequest $request){
		$user = Auth::user();
		$post = new Post();

		$text = $this->reverseFirstWord($request->text);

		$post->text = $text;
		$post->user_id = $user->id;

		$post->save();
		$post->user;

		return $post;
	}

	public function update(PostUpdateRequest $request){
		$post = Post::find($request->post_id);

		$text = $this->reverseFirstWord($request->text);
		$post->text = $text;
		$post->save();

		return $post;
	}

	public function delete(PostDeleteRequest $request){
		$post = Post::find($request->post_id);
		$post->delete();
	}

	public function previous(Request $request){
		$user = Auth::user();
		$last_id = session('last_id');
		$posts = Post::where('id', '<',$last_id)->orderBy('created_at', 'desc')->with('user')->take(10)->get();

		if ($posts->count() > 0) {
            $last_id = $posts->last()->id;

        	$post_not_user = $posts->where('user_id', '<>', $user->id);

			session([
				'posts' => [
					'ids' => array_merge((session('posts'))['ids'], $post_not_user->pluck('id')->toArray()),
					'timestamp' => date('Y-m-d H:i:s')
				],
				'last_id' => $last_id
			]);
		}

		return $posts;
	}

	public function loadChanges(Request $request){
		$user = Auth::user();
		$posts_current = session('posts');
		$id_posts_current = $posts_current['ids'];
		$timestamp = $posts_current['timestamp'];
		$update_timestamp = false;

		$posts_change = Post::whereIn('id', $id_posts_current)
			->where(
				function($query) use ($timestamp){
					$query->where('updated_at', '>', $timestamp);
					$query->orWhere('deleted_at', '>', $timestamp);
				}
			)
			->where('user_id', '<>', $user->id)
			->withTrashed()
			->get();

		$new_posts = Post::where('user_id', '<>', $user->id)->where('created_at', '>', $timestamp)->with('user')->get();

		$data = [
			'update' => [],
			'create' => [],
			'delete' => []
		];

		if ($posts_change->count() > 0) {
			$update_posts = $posts_change->where('updated_at', '>', $timestamp);
			$delete_posts = $posts_change->where('deleted_at', '>', $timestamp);

			if ($update_posts->count() > 0) $data['update'] = $update_posts;

			if ($delete_posts->count() > 0) {
				$data['delete'] = $delete_posts->pluck('id')->toArray();
				$id_posts_current = array_diff($id_posts_current, $data['delete']);
			}

			$update_timestamp = true;
		}

		if ($new_posts->count() > 0) {
			$data['create'] = $new_posts;
			$id_posts_current = array_merge($id_posts_current, $new_posts->pluck('id')->toArray());
			$update_timestamp = true;
		}

		if ($update_timestamp) {
			session([
				'posts' => [
					'ids' => $id_posts_current,
					'timestamp' => date('Y-m-d H:i:s')
				]
			]);
		}

		return response()->json($data);
	}
}
