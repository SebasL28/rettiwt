<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Post;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $posts = Post::orderBy('created_at', 'desc')->take(10)->get();
        $data = [
            "posts" => $posts
        ];

        $last_id = 0;
        if ($posts->count() > 0) {
            $last_id = $posts->last()->id;
        }

        $post_not_user = $posts->where('user_id', '<>', $user->id);

        session([
            'posts' => [
                'ids' => $post_not_user->pluck('id')->toArray(),
                'timestamp' => date('Y-m-d H:i:s')
            ],
            'last_id' => $last_id
        ]);

        return view('home', $data);
    }
}
