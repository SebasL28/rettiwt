<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Home Route
Route::get('/', 'Auth\LoginController@showLoginForm')->name('login.form');

// Login Routes
Route::post('login', 'Auth\LoginController@loginValidate')->name('login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register.form');
Route::post('register', 'Auth\RegisterController@registerValidate')->name('register');


Route::middleware('auth')->group(function () {
	Route::get('home', 'HomeController@index')->name('home');

	Route::post('post/new', 'PostController@store')->name('post.new');
	Route::post('post/update', 'PostController@update')->name('post.update');
	Route::post('post/delete', 'PostController@delete')->name('post.delete');
	Route::get('post/previous', 'PostController@previous')->name('post.previous');
	Route::get('post/load', 'PostController@loadChanges')->name('post.load_changes');
});
