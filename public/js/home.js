	$(document).ready(function(){
		var card = $(".card-board");
		
		TweenLite.set(card, {y:-100});
		TweenLite.to(card, 1, {opacity:1, y:0});

		var id_update = 0,
			id_delete = 0,
			modal_update = $("#modal-update");

		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});

		$('#btn-logout').click(function(e){
			e.preventDefault();
			swal({
				title: 'Cerrar sesión',
				text: '¿Realmente desea cerrar sesión?',
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Si',
				cancelButtonText: 'Cancelar'
			}).then((result) => {
				if (result.value) {
					TweenLite.to(card, 1, {opacity:0, y:-100});
					$('#form-logout').submit();
				}
			});
		});

		$('#new-post-txt').keyup(function(){
			var input = $(this),
				length = input.val().length;

			changeNumberLength(length, 'new');
		});

		$('#update-post-txt').keyup(function(){
			var input = $(this),
				length = input.val().length;

			changeNumberLength(length, 'update');
		});

		$('#form-new-post').submit(function(e){
			e.preventDefault();
			var form = $(this),
				input_text = $('#new-post-txt'),
				text = input_text.val(),
				btn = form.find('button[type=submit]'),
				text_btn = btn.html();

			btn.addClass('disabled').html("<span class='fa fa-spinner fa-pulse'></span>");
			input_text.attr('disabled', 'disabled');
			changeNumberLength(0, 'new');
			$.post(url_new_post, {text: text}, function(data){
				input_text.removeAttr('disabled').val('');
				btn.removeClass('disabled').html(text_btn);
				addPost(data, true, 'prepend');
			});
		});

		$("#form-update-post").submit(function(e){
			e.preventDefault();
			var form = $(this),
				input_text = $('#update-post-txt'),
				text = input_text.val(),
				btn = form.find('button[type=submit]'),
				text_btn = btn.html();

			btn.addClass('disabled').html("<span class='fa fa-spinner fa-pulse'></span>");
			input_text.attr('disabled', 'disabled');
			changeNumberLength(0, 'update');
			$.post(url_update_post, {text: text, post_id: id_update}, function(data){
				input_text.removeAttr('disabled').val('');
				modal_update.modal('hide');
				btn.removeClass('disabled').html(text_btn);
				updatePost(data);
			});
		});

		$('#container-posts').on('click', '.post .btn-update', function(e){
			e.preventDefault();
			var btn = $(this),
				post = btn.closest('.post'),
				text = post.find('.text-post').html(),
				id = post.attr('data-id'),
				input_update = $("#update-post-txt")

			id_update = id;
			input_update.val(text);
			changeNumberLength(text.length, 'update');
			modal_update.modal('show');
		});

		$("#container-posts").on('click', '.post .btn-delete', function(e){
			e.preventDefault();
			var btn = $(this),
				post = btn.closest('.post'),
				id = post.attr('data-id');

			id_delete = id;

			swal({
				title: 'Eliminar post',
				text: '¿Realmente desea eliminar el post?',
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Si',
				cancelButtonText: 'Cancelar'
			}).then((result) => {
				if (result.value) {
					swal({
						title: 'Eliminando post',
						html: '<span style="font-size:30px;" class="fa fa-spinner fa-pulse"></span>',
						showCancelButton: false,
						showConfirmButton: false,
						allowOutsideClick: false,
						allowEscapeKey: false
					});

					$.post(url_delete_post, {post_id: id_delete}, function(){
						deletePost(id_delete);
						swal.close();
					});
				}
			});
		});

		$("#btn-previous").click(function(e){
			e.preventDefault();
			loadPostPrevious();
		});

		loadChanges();
	});

	function changeNumberLength(number, type){
		var input = '';
		if (type == 'new') {
			input = $('#length-new-post span');
		} else if (type == 'update'){
			input = $('#length-update-post span');
		}

		input.html(number);
	}

	function addPost(post, post_own, type_position){
		var container = $('#container-posts'),
			post_empty = container.find('.post-empty');

		var content = '<div class="card-container"><article data-id="'+post.id+'" class="post"><div class="article-header"><div class="row"><div class="col-8"><div class="info-author"><div class="avatar-post rounded-circle"><img class="img-fluid" src="/images/avatar.jpg"></div><p>'+post.user.name+'</p><small>'+post.user.email+'</small></div></div>';

		if (post_own) {
			content += '<div class="col-4 text-right"><div class="dropdown dropleft"><button class="btn btn-menu-post rounded-circle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-h"></i></button><div class="dropdown-menu"><a class="dropdown-item btn-update" href="#">Editar</a><a class="dropdown-item btn-delete" href="#">Eliminar</a></div></div></div>';
		}

		content += '</div></div><p class="text-post">'+post.text+'</p></article></div>';

		if (post_empty.length == 1){
			post_empty.remove();
		}

		if (type_position == 'append') {
			container.append(content);
		}
		else if (type_position == 'prepend'){
			container.prepend(content);
		}
	}

	function updatePost(data){
		var $post = $("article[data-id='"+data.id+"']"),
			text_post = $post.find('.text-post');

		text_post.html(data.text);
	}

	function deletePost(id){
		var container = $("#container-posts"),
			post = $("article[data-id='"+id+"']");
		post.parent().remove();

		if (container.find('.post').length == 0) {
			var content = '<div class="card-container post-empty"><h3 class="text-center">No se encontraron publicaciones</h3></div>';

			container.prepend(content);
		}
	}

	function loadChanges(){
		$.getJSON(url_load_changes, function(data){
			var new_posts = data.create,
				update_posts = data.update,
				delete_posts = data.delete;

			if (new_posts.length > 0) {
				for (var i = 0; i < new_posts.length; i++) {
					addPost(new_posts[i], false, 'prepend');
				}
			}

			if (update_posts.length > 0) {
				for (var i = 0; i < update_posts.length; i++) {
					updatePost(update_posts[i]);
				}
			}

			if (delete_posts.length > 0) {
				for (var i = 0; i < delete_posts.length; i++) {
					deletePost(delete_posts[i]);
				}
			}

			setTimeout(loadChanges, time_load_changes);		
		});
	}

	function loadPostPrevious(){
		$.getJSON(url_previous, function(data){
			if (data.length > 0) {
				for (var i = 0; i < data.length; i++) {
					var property_post = (email_user == data[i].user.email);
					addPost(data[i], property_post, 'append');
				}
			}
			else{
				swal(
					'Mensajes cargados',
					'No se encontraron mensajes anteriores',
					'info'
				);
			}
		});
	}